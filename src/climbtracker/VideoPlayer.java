package climbtracker;

import climbtracker.customui.ImagePanel;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSlider;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;

/**
 * Video player for playing videos using OpenCV and ffmpeg The VideoPlayer
 * contains the ClimbProcessor to processes the frames of the videos.
 *
 * @author Eric
 */
public final class VideoPlayer
    {
    // Missing constants in java version of OpenCV
    // These represent property IDs when using VideoCapture.get
    public static final int CV_CAP_PROP_POS_FRAMES = 1, // Current frame of VideoCapture object
        CV_CAP_PROP_POS_AVI_RATIO = 2, // Position of video with 0 being the beginning and 1 being the end
        CV_CAP_PROP_FPS = 5, // FPS of a video according to VideoCapture
        CV_CAP_PROP_FRAME_COUNT = 7; // Number of frames in a video

    private File videoFile;
    private Thread playbackThread; // New thread that gets spawned when videos are played
    private Playback playback; // A runnable that does the video playback. Gets inserted into playbackThread as an argument when its constructed.

    public VideoPlayer(File videoFile)
        {
        this(); // Initialize all other members
        setVideoFile(videoFile);
        }

    public VideoPlayer()
        {
        this.videoFile = null;
        playbackThread = null;
        playback = null;
        }

    public void setVideoFile(File videoFile)
        {
        if (!VideoPlayer.isValidVideo(videoFile))
            {
            throw new IllegalArgumentException("Video file unopenable via ffmpeg.");
            }

        ClimbTracker.log.finer("VideoPlayer's file set to: " + videoFile.getAbsolutePath());
        this.videoFile = videoFile;
        }

    public void playVideo(ImagePanel panel, JSlider slider, JButton playBtn, JLabel frameLabel)
        {
        // Make sure we have a true video file
        if (videoFile != null && isValidVideo(videoFile))
            {
            ClimbTracker.log.finer("Playing video file.");
            VideoCapture cap = new VideoCapture();
            cap.open(videoFile.getAbsolutePath());

            // Spawn new thread to start the video playback
            playback = new Playback(cap, panel, slider, playBtn, frameLabel);
            playbackThread = new Thread(playback);
            playbackThread.start();
            }
        else // Video file was not set
            {
            throw new IllegalStateException("Video file must be set and valid before attempting to play it.");
            }
        }

    public Dimension getVideoDimensions()
        {
        VideoCapture cap = new VideoCapture();
        cap.open(videoFile.getAbsolutePath());

        int videoWidth = (int) cap.get(Highgui.CV_CAP_PROP_FRAME_WIDTH);
        int videoHeight = (int) cap.get(Highgui.CV_CAP_PROP_FRAME_HEIGHT);

        cap.release();

        return new Dimension(videoWidth, videoHeight);
        }

    public boolean isPlayingVideo()
        {
        if (playbackThread == null)
            return false;
        
        return playbackThread.isAlive();
        }

    public void stopPlayingVideo()
        {
        if (playback != null)
            playback.stop();
        }
    
    public void pausePlayingVideo()
        {
        if (playback != null)
            playback.pause();
        }
    
    public void unPausePlayingVideo()
        {
        if (playback != null)
            playback.unPause();
        }
    
    public boolean isStopped()
        {
        if (playback == null)
            return true;
        else
            return playback.isStopped();
        }
    
    public void setFrame(int frame)
        {
        if (playback == null)
            return;
        playback.setFrame(frame);
        }

    public static boolean isValidVideo(File videoFile)
        {
        VideoCapture cap = new VideoCapture();
        cap.open(videoFile.getAbsolutePath());

        if (!cap.isOpened())
            {
            ClimbTracker.log.warning("Unable to open file given. ");
            return false;
            }
        else
            {
            return true;
            }
        }

    public static BufferedImage Mat2BufferedImage(Mat m)
        {
        if (m == null)
            {
            return null;
            }

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1)
            {
            type = BufferedImage.TYPE_3BYTE_BGR;
            }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
        }
    }
