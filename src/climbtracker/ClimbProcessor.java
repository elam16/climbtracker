package climbtracker;

import climbtracker.customui.ImagePanel;
import java.io.File;
import org.opencv.highgui.VideoCapture;

/**
 * This class processes videos of climbs.
 *
 * @author Eric
 */
public class ClimbProcessor
    {

    public ClimbProcessor()
        {

        }

    public void procVideo(ClimbView view, File vidFile, int bgFrame, int startFrame, int endFrame, ImagePanel imgPanel)
        {
        VideoCapture cap = new VideoCapture();
        boolean opened = cap.open(vidFile.getAbsolutePath());

        if (!opened)
            throw new IllegalArgumentException("Video file given not openable with OpenCV/FFMPEG.");
        
        ProcessorRunnable proc = new ProcessorRunnable(view, cap, bgFrame, startFrame, endFrame, imgPanel);
        Thread thread = new Thread(proc);
        thread.start();
        }
    }
