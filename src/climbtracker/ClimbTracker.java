package climbtracker;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import org.opencv.core.Core;

/**
 * This class contains the main method initializes the ClimbView.
 * The ClimbView is the main GUI window for this application. The ClimbView
 * contains the VideoPlayer to view videos. When a user wants to process a 
 * video of a climb, the user must submit information about certain key frames
 * (see readme.txt for more details). The ClimbView uses the ClimbProcessor
 * to process videos. 
 * 
 * @author Eric
 */
public class ClimbTracker
    {
    
    public static final Logger log = Logger.getLogger(ClimbTracker.class.getName()); // Logger
    private static final Level LOG_LEVEL = Level.WARNING; // Current logger level
    
    /**
     * Path to FFMPEG dll
     */
    public static final String FFMPEG_DLL_PATH = "./lib/opencv_ffmpeg2410_64";
    
    /**
     * Path to OpenCV dll
     */
    public static final String OPENCV_DLL_PATH = "./lib/" + Core.NATIVE_LIBRARY_NAME;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
        {
        // Setup logger
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter());
        handler.setLevel(LOG_LEVEL);
        log.setLevel(LOG_LEVEL);
        log.addHandler(handler);
        
        // Load native DLLs
        System.loadLibrary(OPENCV_DLL_PATH); // Load OpenCV
        System.loadLibrary(FFMPEG_DLL_PATH); // Load FFMPEG
        log.finer("DLLs loaded");
        
        // Initialize the view
        ClimbView view = new ClimbView();
        log.finer("ClimbView initialized");
        }
    }
