package climbtracker;

import climbtracker.customui.ImagePanel;
import static climbtracker.VideoPlayer.Mat2BufferedImage;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSlider;
import org.opencv.core.Mat;
import org.opencv.highgui.VideoCapture;

/**
 * A runnable for playing videos. This is used to play videos in new thread.
 *
 * @author Eric
 */
public class Playback implements Runnable
    {
    private final int PAUSE_SLEEP_TIME = 100; // Sleep time during video pausing so the thread doesn't constantly run

    private VideoCapture cap; // VideoCapture object to get frames from
    private ImagePanel panel; // Panel to display image on
    private JButton playBtn; // Play button
    private JSlider slider; // Slider to indicate progress
    private JLabel frameLabel; // Label to indicate what frame we're on
    private boolean stop, // Flag indicating that an instance of Playback should stop running
            pause, // Flag indiciating that the video being played should pause
            setFrameReq; // Flag indicating that a request has been set to set the current frame of the video
    private int currFrame;

    /**
     * Constructor for Playback
     *
     * @param cap VideoCapture to get frames from.
     * @param panel Panel to put video frames onto
     * @param slider Slider to update when progressing through frames
     * @param playBtn Button to update when video is done
     * @param frameLabel Label to update to show current frame
     */
    public Playback(VideoCapture cap, ImagePanel panel, JSlider slider, JButton playBtn, JLabel frameLabel)
        {
        this.cap = cap;
        this.panel = panel;
        this.playBtn = playBtn;
        this.slider = slider;
        this.frameLabel = frameLabel;
        stop = true;
        pause = false;
        setFrameReq = false;
        currFrame = 0;
        }

    @Override
    public void run()
        {
        stop = false;
        setFrameReq = false;
        
        int numFrames = (int) cap.get(VideoPlayer.CV_CAP_PROP_FRAME_COUNT); // Get number of frames in video
        int fps = (int) cap.get(VideoPlayer.CV_CAP_PROP_FPS);
        ClimbTracker.log.finer("Playing video with " + numFrames + " frames.");
        ClimbTracker.log.finer("FPS: " + fps);

        // Setup the slider
        this.slider.setMinimum(0);
        this.slider.setMaximum(numFrames-1);
        
        Mat frame = new Mat(); // Mat to hold the current frame data

        long pauseTime = 1000/fps; // Time that should pass between each frame
        long beginTime;
        long timePassed;
        currFrame = 0;
        while (currFrame < numFrames)
            {
            beginTime = System.currentTimeMillis(); // Record the start time of the loop
            
            // Get frame
            cap.grab();
            cap.retrieve(frame);
            
            // Display image
            panel.setImage(Mat2BufferedImage(frame));
            panel.repaint();
            
            handlePause(); // Handle pauses if a pause is requested
            if (stop) { stopPlayback(); return; } // Stop playing the video if requested
            slider.setValue(currFrame); // Update the slider
            frameLabel.setText(Integer.toString(currFrame)); // Update current frame
            currFrame++;
            
            timePassed = System.currentTimeMillis() - beginTime; // Calculate the time it took to do the current iteration of the loop
            if (timePassed < pauseTime) // If the time it took to do one iteration of the loop is less than the time between displaying frames
                {
                try { Thread.sleep(pauseTime - timePassed); } // Pause for the remaining time between frames
                catch (InterruptedException e) {}
                }
            }

        ClimbTracker.log.finer("Done playing");
        stopPlayback();
        }

    /**
     * Submit a stop request to the video that is currently playing
     */
    public void stop()
        {
        stop = true;
        }

    /**
     * Submit a pause request to the video that is currently playing
     */
    public void pause()
        {
        pause = true;
        }

    /**
     * Submit a unpause request to the video that is currently playing. This
     * isn't called "play" because this method won't play a video if it hasn't
     * started yet.
     */
    public void unPause()
        {
        pause = false;
        }

    /**
     * Returns whether the current playback has been stopped because it reached
     * the end or it was stopped via calling the stop method
     * @return 
     */
    public boolean isStopped()
        {
        return stop;
        }
    
    /**
     * This method will trigger a request to set the frame of the current video
     * playing. The request gets handled after a pause request has been sent.
     * 
     * @param frame Frame number to play.
     */
    public void setFrame(int frame)
        {
        this.currFrame = frame;
        this.setFrameReq = true;
        }

    /**
     * Helper method to handle the various flags that can be triggered. E.g.: If
     * something calls Playback.stop(), then a pause flag is triggered, meaning
     * the video that's being played should be paused
     */
    private void handlePause()
        {
        if (pause) // If a pause was requetsed
            {
            ClimbTracker.log.finer("Pausing Playback. ");
            // Delay thread here until... 
            while (true)
                {
                try
                    {
                    if (stop) // If a stop request was put in
                        {
                        stopPlayback();
                        return;
                        }
                    else if (!pause) // If we were told to resume
                        return;
                    else if (setFrameReq) // If we were told to set the current frame
                        {
                        handleSetFrame();
                        }
                    Thread.sleep(PAUSE_SLEEP_TIME); // Delay thread by SLEEP_TIME
                    }
                catch (InterruptedException ex) { }
                }
            }
        }

    /**
     * Handle request to set the current frame of the video
     */
    private void handleSetFrame()
        {
        cap.set(VideoPlayer.CV_CAP_PROP_POS_FRAMES, currFrame); // Set the current frame in the VideoCapture object

        // Retrieve the frame
        Mat frame = new Mat();
        cap.grab();
        cap.retrieve(frame);
        
        // Display frame on the panel
        panel.setImage(Mat2BufferedImage(frame));
        panel.repaint();
        
        // Reset flag to indicate request has been done
        setFrameReq = false;
        }
    
    /**
     * Helper function to clean up
     */
    private void stopPlayback()
        {
        ClimbTracker.log.finer("Stopping Playback. ");
        if (cap.isOpened())
            {
            ClimbTracker.log.finer("VideoCapture Released. ");
            cap.release();
            }
        stop = true;
        this.playBtn.setText(ClimbView.TXT_BTN_PLAY);
        }
    }
