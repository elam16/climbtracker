package climbtracker;

import climbtracker.customui.ImagePanel;
import climbtracker.customui.VideoSlider;
import climbtracker.viewlisteners.BtnFileChooserListener;
import climbtracker.viewlisteners.BtnKeyFrameListener;
import climbtracker.viewlisteners.BtnPlayListener;
import climbtracker.viewlisteners.BtnProcessListener;
import climbtracker.viewlisteners.SliderChangeListener;
import climbtracker.viewlisteners.SliderMouseListener;
import climbtracker.viewlisteners.BtnMoveFrameListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * This class is the main window for the GUI of this application.
 *
 * @author Eric
 */
public class ClimbView extends JFrame
    {
    public static final String TXT_BTN_FILE_CHOOSER = "Choose Video";
    public static final String TXT_BTN_PLAY = "Play";
    public static final String TXT_BTN_PAUSE = "Pause";
    public static final String TXT_BTN_PROCESS = "Process";
    public static final String TXT_BTN_BG_FRAME = "Background Frame";
    public static final String TXT_BTN_START_FRAME = "Start Frame";
    public static final String TXT_BTN_END_FRAME = "End Frame";
    public static final String TXT_BTN_FORWARD_FRAME = ">";
    public static final String TXT_BTN_BACK_FRAME = "<";

    public static final String TXT_INVALID_VID_FILE = "This video file cannot be opened. Please choose another one. ";
    public static final String TXT_FRAME_START_AFTER_END = "Unable to process video. The Start Frame must be before the end frame. ";
    public static final String TXT_FRAME_MISSING = "Unable to process video. You must mark a background, start and end frame. ";
    
    private static final int BTN_GAP = 5;
    
    private JButton btnFileChooser,
            btnPlay, // Play/pause button for videos
            btnProcess,
            btnChooseBgFrame, btnChooseStartFrame, btnChooseEndFrame, // Buttons for the user to choose key frames
            btnForwardFrame, btnBackFrame; // Buttons to go forward/backwards a frame
    private VideoSlider vidSlider;
    private JPanel bottomPanel, btnPanel, sliderPanel;
    private JLabel frameLabel;
    private ImagePanel imgPanel;

    private BoxLayout layout;

    private File videoFile;

    private VideoPlayer player;
    private ClimbProcessor proc;

    private boolean videoFileChanged;

    public ClimbView()
        {
        super();
        videoFileChanged = false;

        player = new VideoPlayer(); // Initialize video player
        proc = new ClimbProcessor(); // Initialize Climb Processor
        
        // Initialize the panel which will display images
        imgPanel = new ImagePanel();

        // Initialize panels which hold various components
        bottomPanel = new JPanel();
        btnPanel = new JPanel();
        sliderPanel = new JPanel();

        // Initialize labels
        frameLabel = new JLabel(); // This will hold the current frame of the video
        
        // Initialize slider
        vidSlider = new VideoSlider(0, 100, 0);
        vidSlider.addMouseListener(new SliderMouseListener(this));
        vidSlider.addChangeListener(new SliderChangeListener(frameLabel));
        
        // Initialize butons and their listeners
        btnFileChooser = new JButton(TXT_BTN_FILE_CHOOSER);
        btnFileChooser.addActionListener(new BtnFileChooserListener(this));
        btnPlay = new JButton(TXT_BTN_PLAY);
        btnPlay.addActionListener(new BtnPlayListener(this));
        btnProcess = new JButton(TXT_BTN_PROCESS);
        btnProcess.addActionListener(new BtnProcessListener(this));
        BtnMoveFrameListener moveFrameLis = new BtnMoveFrameListener(this, vidSlider);
        btnForwardFrame = new JButton(TXT_BTN_FORWARD_FRAME);
        btnForwardFrame.addActionListener(moveFrameLis);
        btnBackFrame = new JButton(TXT_BTN_BACK_FRAME);
        btnBackFrame.addActionListener(moveFrameLis);
        
        // Initialize buttons for choosing key frames
        BtnKeyFrameListener keyFrameList = new BtnKeyFrameListener(vidSlider);
        btnChooseBgFrame = new JButton(TXT_BTN_BG_FRAME);
        btnChooseBgFrame.addActionListener(keyFrameList);
        btnChooseStartFrame = new JButton(TXT_BTN_START_FRAME);
        btnChooseStartFrame.addActionListener(keyFrameList);
        btnChooseEndFrame = new JButton(TXT_BTN_END_FRAME);
        btnChooseEndFrame.addActionListener(keyFrameList);
        
        // Add components to the frame
        this.setLayout(new BorderLayout());
        this.add(imgPanel, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);

        // Add components to the bottom panel
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.add(btnPanel, BorderLayout.SOUTH);
        bottomPanel.add(sliderPanel, BorderLayout.CENTER);
        
        // Add components to the slider panel
        sliderPanel.add(frameLabel);
        sliderPanel.add(vidSlider);
        sliderPanel.add(btnBackFrame);
        sliderPanel.add(btnForwardFrame);

        // Add buttons to the button panel
        btnPanel.setLayout(new GridLayout(2, 3, BTN_GAP, BTN_GAP));
        btnPanel.add(btnPlay);
        btnPanel.add(btnProcess);
        btnPanel.add(btnFileChooser);
        btnPanel.add(btnChooseBgFrame);
        btnPanel.add(btnChooseStartFrame);
        btnPanel.add(btnChooseEndFrame);

        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        }

    /**
     *
     * @return True when a file is successfully chosen, else, false.
     */
    public boolean chooseVideoFile()
        {
        pauseVideo();

        FileNameExtensionFilter filter;
        filter = new FileNameExtensionFilter("MP4 File (.mp4)", "mp4");
        
        boolean validChosenVideo = false;
        JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
        fileChooser.setFileFilter(filter);
        int fcRetVal = fileChooser.showOpenDialog(this);

        if (fcRetVal == JFileChooser.APPROVE_OPTION) // If a file was chosen
            {
            File chosenFile = fileChooser.getSelectedFile();
            ClimbTracker.log.finer("Chosen file: " + chosenFile.getAbsolutePath());
            if (VideoPlayer.isValidVideo(chosenFile))
                {
                this.videoFile = chosenFile;
                validChosenVideo = true;
                videoFileChanged = true;
                stopVideo();
                }
            else // If invalid video file
                {
                JOptionPane.showMessageDialog(this, TXT_INVALID_VID_FILE);
                }
            }

        return validChosenVideo;
        }

    public File getVideoFile()
        {
        return videoFile;
        }

    public void playVideo()
        {
        // If the user hasn't changed video files, then that means the user
        // wants to resume the currently playing video
        if (!videoFileChanged && !player.isStopped())
            {
            ClimbTracker.log.finer("Unpausing video");
            player.unPausePlayingVideo();
            }
        else // Else, go through the process to play a new video
            {
            // If the code reaches this point, the user wants to play a video file
            // from the beginning
            ClimbTracker.log.finer("Playing video. ");

            if (videoFile == null) // If no video file was chosen yet
                {
                if (!chooseVideoFile()) // And if user did not choose a proper video
                    {
                    return; // Then return because we can't proceed without a proper video
                    }
                }

            // If playVideo is called, stopping the video will be done first
            // but within the playVideo method itself. (Implementation for this below)
            if (player.isPlayingVideo()) // If we have a video already playing
                {
                player.stopPlayingVideo(); // Signal the video player to stop it
                }

            // Hold up until the player fully stops playing the video
            while (player.isPlayingVideo())
                {
                ClimbTracker.log.finer("Waiting for player to stop playing video");
                }

            player.setVideoFile(videoFile);

            // Resize components appropriately
            Dimension dim = player.getVideoDimensions();

            // Get how much we need to increase/decrease the frame by
            int newFrameWidth = this.getWidth() + (dim.width - imgPanel.getWidth());
            int newFrameHeight = this.getHeight() + (dim.height - imgPanel.getHeight());
            Dimension newFrameDim = new Dimension(newFrameWidth, newFrameHeight);

            imgPanel.setSize(dim); // Set the dimensions of the image panel
            this.setSize(newFrameDim); // Set the dimensions of the frame

            player.playVideo(imgPanel, vidSlider, btnPlay, frameLabel); // Play the video

            videoFileChanged = false; // Reset video file changed flag
            vidSlider.resetKeyFrames();
            }
        btnPlay.setText(TXT_BTN_PAUSE); // Switch the text of the Play Button back to Pause
        }

    public void pauseVideo()
        {
        ClimbTracker.log.finer("Pausing video. ");
        player.pausePlayingVideo();
        btnPlay.setText(TXT_BTN_PLAY); // Switch the text of the Play Button back to Play
        }

    public void processVideo()
        {
        ClimbTracker.log.finer("Processing video. ");
        player.pausePlayingVideo();
        
        if (vidSlider.getBgFrame() == -1 || vidSlider.getStartFrame() == -1 || vidSlider.getEndFrame() == -1)
            {
            JOptionPane.showMessageDialog(this, TXT_FRAME_MISSING); 
            return;
            }
            
        if (vidSlider.getStartFrame() >= vidSlider.getEndFrame())
            {
            JOptionPane.showMessageDialog(this, TXT_FRAME_START_AFTER_END); 
            return;
            } 
            
        player.stopPlayingVideo();
        disableUI(); // Disable the UI so user can't do anything during processing
        
        // Start the video processing
        proc.procVideo(this, videoFile, vidSlider.getBgFrame(), vidSlider.getStartFrame(), vidSlider.getEndFrame(), imgPanel);
        }

    public void stopVideo()
        {
        ClimbTracker.log.finer("Stopping video. ");
        player.stopPlayingVideo();
        }
    
    public void setFrame(int frame)
        {
        ClimbTracker.log.finer("Setting frame to: " + frame);
        player.setFrame(frame);
        }
    
    /**
     * Disable UI so a user cannot press buttons or manipulate other components
     */
    public void disableUI()
        {
        btnFileChooser.setEnabled(false);
        btnPlay.setEnabled(false);
        btnProcess.setEnabled(false);
        btnChooseBgFrame.setEnabled(false);
        btnChooseStartFrame.setEnabled(false);
        btnChooseEndFrame.setEnabled(false);
        btnForwardFrame.setEnabled(false);
        btnBackFrame.setEnabled(false);
        vidSlider.setEnabled(false);
        }
    
    /**
     * Enable the UI so the user can press buttons or manipulate other components
     */
    public void enableUI()
        {
        btnFileChooser.setEnabled(true);
        btnPlay.setEnabled(true);
        btnProcess.setEnabled(true);
        btnChooseBgFrame.setEnabled(true);
        btnChooseStartFrame.setEnabled(true);
        btnChooseEndFrame.setEnabled(true);
        btnForwardFrame.setEnabled(true);
        btnBackFrame.setEnabled(true);
        vidSlider.setEnabled(true);
        }
    }
