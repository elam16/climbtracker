package climbtracker;

import climbtracker.customui.ImagePanel;
import java.util.ArrayList;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

/**
 * A class that actually does the image processing and implements runnable
 * so that the processing can be done in another thread so that it doesn't
 * take up the UI thread.
 * 
 * @author Eric
 */
public class ProcessorRunnable implements Runnable
    {

    private ClimbView view; // ClimbView object so that we can reenable the UI after the conclusion of this process
    private VideoCapture cap; // VideoCapture object with a video file already loaded in
    private ImagePanel imgPanel; // Panel to draw resulting images onto
    
    private int bgFrame, startFrame, endFrame; // Key frames for processing

    public ProcessorRunnable(ClimbView view, VideoCapture cap, int bgFrame, int startFrame, int endFrame, ImagePanel imgPanel)
        {
        this.view = view;
        this.cap = cap;
        this.imgPanel = imgPanel;
        this.bgFrame = bgFrame;
        this.startFrame = startFrame;
        this.endFrame = endFrame;
        }

    @Override
    public void run()
        {
        ClimbTracker.log.finer("Starting processing.");        
        
        // Get background frame
        Mat bgMat = new Mat();
        cap.set(VideoPlayer.CV_CAP_PROP_POS_FRAMES, bgFrame); // Set the video back to the frame where the background is
        cap.grab();
        cap.retrieve(bgMat);
        
        Mat resultMat = bgMat.clone();

        // Convert background to a gray scaled version
        Mat bgMatGray = new Mat();
        Imgproc.cvtColor(bgMat, bgMatGray, Imgproc.COLOR_BGR2GRAY);

        cap.set(VideoPlayer.CV_CAP_PROP_POS_FRAMES, startFrame); // Set the video back to the first frame
        Mat currMat = new Mat(); // Current frame
        Mat currMatGray = new Mat(); // Gray version of current frame
        Mat diff = new Mat();
        Mat thresh = new Mat();
        Mat hier = new Mat();
        for (int i = startFrame; i <= endFrame; i++) // Loop through the desired frames
            {
            // Grab current frame
            cap.grab();
            cap.retrieve(currMat);
            Imgproc.cvtColor(currMat, currMatGray, Imgproc.COLOR_BGR2GRAY); // Gray scale current frame
            
            Core.absdiff(currMatGray, bgMatGray, diff); // Difference the current frame and the background frame
            Imgproc.threshold(diff, thresh, 20, 255, Imgproc.THRESH_BINARY); // Threshold the frame
            Imgproc.blur(thresh, thresh, new Size(5, 5)); // Get rid of noise via blurring
            Imgproc.threshold(thresh, thresh, 20, 255, Imgproc.THRESH_BINARY); // Threshold the frame again

            ArrayList<MatOfPoint> contours = new ArrayList<>();
            Imgproc.findContours(thresh, contours, hier, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE); // Get contours of the processed image
            Rect rect = getLargestRect(contours); // Get the largest contour's rectangle
            if (rect != null) // If we have a rectangle available
                {
                Core.circle(resultMat, new Point(rect.x + rect.width/2, rect.y + rect.height/2), 
                        3, new Scalar(0, 255, 0), 3); // Draw the rectangle's center onto the Mat that will be returned
                }
            
            // Only display the current progress of the processing at certain intervals to save resources
            if (i%((endFrame - startFrame)/10) == 0 && rect != null) 
                {
                Core.rectangle(currMat, new Point(rect.x, rect.y), 
                        new Point(rect.x + rect.width, rect.y + rect.height), 
                        new Scalar(0, 0, 255), 3); // Draw the resulting rectangle
                Core.circle(currMat, new Point(rect.x + rect.width/2, rect.y + rect.height/2), 
                            3, new Scalar(0, 255, 0), 3);
                imgPanel.setImage(VideoPlayer.Mat2BufferedImage(currMat));
                imgPanel.repaint();
                }
            }
        
        imgPanel.setImage(VideoPlayer.Mat2BufferedImage(resultMat));
        imgPanel.repaint();
        
        cap.release();
        view.enableUI();
        
        ClimbTracker.log.finer("Done processing.");
        }
    
    /**
     * Helper function to get largest contour's rectangle
     * @param contours array filled with contours
     * @return Largest contour's rectangle
     */
    private Rect getLargestRect(ArrayList<MatOfPoint> contours)
        {
        if (contours == null || contours.isEmpty())
            return null;
        
        MatOfPoint biggestContour = contours.get(0);
        for (MatOfPoint c : contours)
            {
            if (Imgproc.contourArea(biggestContour) < Imgproc.contourArea(c))
                {
                biggestContour = c;
                }
            }
        
        return Imgproc.boundingRect(biggestContour);
        }
    }
