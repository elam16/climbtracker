package climbtracker.viewlisteners;

import climbtracker.ClimbTracker;
import climbtracker.ClimbView;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JSlider;

/**
 * Mouse listener for the slider that shows video progress and allows
 * video scrubbing. 
 * 
 * @author Eric
 */
public class SliderMouseListener implements MouseListener
    {

    ClimbView view;
    
    public SliderMouseListener(ClimbView view)
        {
        this.view = view;
        }
    
    @Override
    public void mouseClicked(MouseEvent e)
        {
        }

    @Override
    public void mousePressed(MouseEvent e)
        {
        JSlider src = (JSlider) e.getSource();
        ClimbTracker.log.finer("Slider clicked: " + src.getValue());
        view.pauseVideo();
        }

    @Override
    public void mouseReleased(MouseEvent e)
        {
        JSlider src = (JSlider) e.getSource();
        ClimbTracker.log.finer("Slider released: " + src.getValue());
        view.setFrame(src.getValue());
        }

    @Override
    public void mouseEntered(MouseEvent e)
        {
        }

    @Override
    public void mouseExited(MouseEvent e)
        {
        }
    
    }
