package climbtracker.viewlisteners;

import climbtracker.ClimbView;
import climbtracker.ClimbTracker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Button listener for the "Process" button. When pressed, methods are called
 * back to the ClimbView which handles things appropriately. Users should
 * press this button with the intent to process an inputted video and
 * get an image showing the route of the climb.
 * 
 * @author Eric
 */
public class BtnProcessListener implements ActionListener
    {
    
    private ClimbView view; // View to send commands to when button is pressed
    
    public BtnProcessListener(ClimbView view)
        {
        this.view = view;
        }

    @Override
    public void actionPerformed(ActionEvent e)
        {
        ClimbTracker.log.finer("Process button clicked.");
        view.processVideo();
        }
    }
