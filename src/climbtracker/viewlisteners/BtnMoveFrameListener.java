package climbtracker.viewlisteners;

import climbtracker.ClimbTracker;
import climbtracker.ClimbView;
import climbtracker.customui.VideoSlider;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Button listener for the buttons that the video move forward or backward a frame.
 * 
 * @author Eric
 */
public class BtnMoveFrameListener implements ActionListener
    {

    ClimbView view;
    VideoSlider slider;
    
    public BtnMoveFrameListener(ClimbView view, VideoSlider slider)
        {
        this.view = view;
        this.slider = slider;
        }
    
    @Override
    public void actionPerformed(ActionEvent e)
        {
        String btn = e.getActionCommand();
        ClimbTracker.log.finer("Button Clicked " + btn);
        
        int sliderValue = slider.getValue();
        if (btn.equals(ClimbView.TXT_BTN_FORWARD_FRAME) && sliderValue < slider.getMaximum())
            {
            sliderValue++;
            ClimbTracker.log.finer("Moving foward a frame to: " + sliderValue);
            slider.setValue(sliderValue);
            view.pauseVideo();
            view.setFrame(sliderValue);
            }
        else if (btn.equals(ClimbView.TXT_BTN_BACK_FRAME) && sliderValue > slider.getMinimum())
            {
            sliderValue--;
            ClimbTracker.log.finer("Moving back a frame to: " + sliderValue);
            slider.setValue(sliderValue);
            view.pauseVideo();
            view.setFrame(sliderValue);
            }
        }
    
    }
