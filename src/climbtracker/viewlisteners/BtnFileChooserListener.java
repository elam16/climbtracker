package climbtracker.viewlisteners;

import climbtracker.ClimbView;
import climbtracker.ClimbTracker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Button listener for the "Choose File" button. When pressed, methods are called
 * back to the ClimbView which handles things appropriately. Users should press
 * this button with the intent of choosing a video file to either process
 * or play.
 * 
 * @author Eric
 */
public class BtnFileChooserListener implements ActionListener
{
    private ClimbView view;
    
    public BtnFileChooserListener(ClimbView view)
    {
        this.view = view;
    }

    @Override
    public void actionPerformed(ActionEvent ae) 
    {
        ClimbTracker.log.finer("Button file chooser clicked.");
        view.chooseVideoFile();
    }
}
