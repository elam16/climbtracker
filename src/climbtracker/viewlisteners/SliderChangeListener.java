package climbtracker.viewlisteners;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * ChangeListener for updating the current value of the slider onto a label
 * so that the user can see what frame we're at.
 * 
 * @author Eric
 */
public class SliderChangeListener implements ChangeListener
    {

    // Label to update when this slider changes values
    private JLabel label;
    
    public SliderChangeListener(JLabel label)
        {
        this.label = label;
        }
    
    @Override
    public void stateChanged(ChangeEvent e)
        {
        JSlider slider = (JSlider) e.getSource();
        this.label.setText(Integer.toString(slider.getValue()));
        }
    
    }
