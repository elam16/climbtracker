package climbtracker.viewlisteners;

import climbtracker.ClimbTracker;
import climbtracker.ClimbView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Button listener for the "Play" button. When pressed, the button listener
 * will issue a "Play" command if "Pause" was done last or a "Pause" command
 * if "Play" was done last.
 * 
 * @author Eric
 */
public class BtnPlayListener implements ActionListener
    {
    
    private ClimbView view;

    public BtnPlayListener(ClimbView view)
        {
        this.view = view;
        }

    @Override
    public void actionPerformed(ActionEvent e)
        {            
        String btnText = e.getActionCommand(); // Get the command that was done last via the button's text
        
        if (btnText == null)
            throw new IllegalStateException("Cannot retrieve Play/Pause button text via getActionCommand");
        
        if (btnText.equals(ClimbView.TXT_BTN_PLAY)) // If "Play" was done last
            {
            ClimbTracker.log.finer("Play button clicked.");
            view.playVideo();
            }
        else if (btnText.equals(ClimbView.TXT_BTN_PAUSE))
            {
            ClimbTracker.log.finer("Pause button clicked.");
            view.pauseVideo();
            }
        }
    }
