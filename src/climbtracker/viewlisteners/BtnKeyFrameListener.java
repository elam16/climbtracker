package climbtracker.viewlisteners;

import climbtracker.ClimbTracker;
import climbtracker.ClimbView;
import climbtracker.customui.VideoSlider;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Button listener for the buttons that choose key frames of a video. When a
 * user presses one of those buttons, a key frame is marked in the video which
 * will be used in the video's processing.
 *
 * @author Eric
 */
public class BtnKeyFrameListener implements ActionListener
    {

    private VideoSlider slider;

    public BtnKeyFrameListener(VideoSlider slider)
        {
        this.slider = slider;
        }

    @Override
    public void actionPerformed(ActionEvent e)
        {
        // If a video is loaded up

        String btn = e.getActionCommand(); // Figure out which button was pressed
        ClimbTracker.log.finer("Button pressed: " + btn);

        switch (btn)
            {
            case ClimbView.TXT_BTN_BG_FRAME:
                slider.setBgFrame(slider.getValue());
                ClimbTracker.log.finer("Setting bg Frame to frame: " + slider.getValue());
                break;
            case ClimbView.TXT_BTN_START_FRAME:
                slider.setStartFrame(slider.getValue());
                ClimbTracker.log.finer("Setting start Frame to frame: " + slider.getValue());
                break;
            case ClimbView.TXT_BTN_END_FRAME:
                slider.setEndFrame(slider.getValue());
                ClimbTracker.log.finer("Setting end Frame to frame: " + slider.getValue());
                break;
            }
        }

    }
