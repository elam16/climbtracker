package climbtracker.customui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * Panel to show images/videos
 * 
 * @author Eric
 */
public class ImagePanel extends JPanel
    {

    private BufferedImage img;
    
    public ImagePanel()
        {
        super();
        }
    
    public void setImage(BufferedImage img)
        {
        this.img = img;
        }
    
    @Override
    public void paintComponent(Graphics g)
        {
        g.drawImage(img, 0, 0, null);
        }
    }
