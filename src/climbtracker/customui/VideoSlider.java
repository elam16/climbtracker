package climbtracker.customui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JSlider;

/**
 * Custom slider that will display the current progress of a video playing and
 * display key frames chosen by the user.
 *
 * @author Eric
 */
public class VideoSlider extends JSlider
    {

    private static final Color BG_FRAME_COLOR = Color.RED;
    private static final Color START_FRAME_COLOR = Color.GREEN;
    private static final Color END_FRAME_COLOR = Color.BLUE;

    private static final int MARKER_WIDTH = 4;
    private static final int MARKER_HEIGHT = 15;

    private int bgFrame;
    private int startFrame;
    private int endFrame;

    public VideoSlider(int min, int max, int value)
        {
        super(min, max, value);
        bgFrame = -1;
        startFrame = -1;
        endFrame = -1;
        
        }

    public void setBgFrame(int bgFrame)
        {
        this.bgFrame = bgFrame;
        this.repaint();
        }

    public void setStartFrame(int startFrame)
        {
        this.startFrame = startFrame;
        this.repaint();
        }

    public void setEndFrame(int endFrame)
        {
        this.endFrame = endFrame;
        this.repaint();
        }
    
    public int getBgFrame()
        {
        return bgFrame;
        }
    
    public int getStartFrame()
        {
        return startFrame;
        }
    
    public int getEndFrame()
        {
        return endFrame;
        }
    
    public void resetKeyFrames()
        {
        bgFrame = -1;
        startFrame = -1;
        endFrame = -1;
        this.repaint();
        }

    @Override
    protected void paintComponent(Graphics g)
        {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        
        if (bgFrame != -1)
            {
            g2.setColor(BG_FRAME_COLOR);
            g2.fillOval(getKeyFramePos(bgFrame), 0, MARKER_WIDTH, MARKER_HEIGHT);
            }

        if (startFrame != -1)
            {
            g2.setColor(START_FRAME_COLOR);
            g2.fillOval(getKeyFramePos(startFrame), 0, MARKER_WIDTH, MARKER_HEIGHT);
            }

        if (endFrame != -1)
            {
            g2.setColor(END_FRAME_COLOR);
            g2.fillOval(getKeyFramePos(endFrame), 0, MARKER_WIDTH, MARKER_HEIGHT);
            }
        }
    
    // Helper function to calculate x position of where the key frame should be painted
    private int getKeyFramePos(int keyFrame)
        {
        return (int)(((double)keyFrame)/((double)this.getMaximum())*this.getWidth());
        }
    }
