Eric Lam
ECEC 301 Independent Project
Project Name: ClimbTracker

Description: This program processes a video of someone rock climbing and outputs an image of the path the person took while climbing.

OpenCV (http://opencv.org/) was used throughout this project to aid in image processing and video playback. The bulk of the code where OpenCV is used is in:
Playback.java to aid in Video playback
ProcessorRunnable.java to aid in image processing

Startup Information: 
This program relies on OpenCV which relies on native libraries. This copy of ClimbTracker was built with OpenCV for Windows 64-bit and has only been tested on a Windows 64-bit machine. The dependencies for this project are inside the lib folder of the top directory. In order to run the the jar file that is built and placed inside the dist folder, a lib folder containing the dependencies must be contained in the same directory as the jar file. 


Running ClimbTracker:
The program can be started up by opening the project in NetBeans and executing the main class from there or running the .jar file in the dist folder (with the lib folder dependencies). 

After starting up the program, a video must be chosen. Currently only .mp4 files are supported. The input video must be a still video of someone climbing and has a clear picture of the climbing area without a climber in one point of the video. There should be only one person visible throughout the video. A sample is provided in the samples folder in the top directory. Three key pieces of information are needed in order to process the video. 

They are the:
1. Background Frame - a frame where the climbing area is visible without the presence of the climber
2. Start Frame - the frame where you want to start processing the video
3. End Frame - the frame where you want to stop processing the video

In order to choose these frames, play the video and pause at the point in time where the background/start/end frame is appropriate. To find the appropriate frame, the slider can also be dragged around. Once paused at the appropriate frame, press appropriate "____" frame button. This designates the current frame as either a background or start or end frame (whichever was chosen). Repeat the steps until you have chosen a background frame, start frame and an end frame. Once that is done, press the "Process Video" button. The video will start processing. Once the processing is done, the output image is shown on the application. The path of the climber from the climber's position in the start frame to the climber's position in the end frame should be shown. 

A sample input using the ClimbSample.mp4 file would be:

Setting the Background Frame to Frame 0
Setting the Start Frame to Frame 263
Setting the End Frame to Frame 1455

And then processing the video.

Known Problems:
1. When choosing the key frames, the marker that appears on the slider does not always accurately get painted in the right position. 
2. There is a known issue when testing the project with at least the computers in Bossone. When opening the project in NetBeans, the project indicates that the "JDK_1.8" platform could not be found. To resolve this, in NetBeans, right click the ClimbTracker project in the Projects window and press "Resolve Project Problems". Then press Resolve, and then add JDK 1.8 as a platform (leaving the platform name as "JDK 1.8"). The project should work at this point. 