import climbtracker.VideoPlayer;
import org.junit.Test;

/**
 *
 * @author Eric
 */
public class VideoPlayerTests
    {
    
    @Test(expected=IllegalArgumentException.class)
    public void createVideoPlayerWithInvalidVideo()
        {
        VideoPlayer player = new VideoPlayer(null);
        }
    }
